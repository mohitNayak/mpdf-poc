<?php


//use Mpdf\Mpdf;
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

require_once __DIR__ . '/vendor/autoload.php';

$html = '<h1>Hello world!<img src="https://ideaprism.net/Images/idea-logo.jpg" /></h1>';
$logoImage= "http://api.ideaprism.net/uploadImages/1585313204825.jpg";
$title= "Test Dialog";
$titleQuestion= "This is test triggered question..";
$dialogMemberNumber= 2;
$dialogIdeaNumber= 5;
$ReportType=2;

$html ="";

// create some HTML content
$html .= '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        
    <title>Idea1</title>
</head>
<body style="margin: 0px; padding: 0px;">
    <div style="width: 900px; margin: 0px  auto; background-color: #FFFFFF;">
        <div style="background-color: #FFFFFF; padding-top: 0px; padding-left:0px; padding-right: 0px; width: 890px; margin: 0px auto; padding-bottom: 0px;">
            <div style="background-color: #666666; display: inline-block;width:100%;">
                <div style="width:190px; float:left;">
                <img src="'.$logoImage.'" style="margin-left:0px; border-left:0px solid white;border-right:0px solid white; width:190px; height:auto;">
                </div>
                <div style="width: calc(100% - 190px); display:inline-block;float:left; margin-right: 35px; margin-left: 35px;margin-top: 10px;">
                    <h3 style="padding-top:10px; margin-bottom: 7px; color: #FFFFFF; font-size: 16px; font-family: Arial, Helvetica, sans-serif;">'.$title.'</h3>
                    <div style="height:1px; width: 100%; background-color: #ffffff;"></div>
                    <p style="margin: 5px 0px; padding: 0px 0px 6px 0px; color: #FFFFFF; font-size: 16px; font-family: Arial, Helvetica, sans-serif;">'.$titleQuestion.'</p>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div style="width: 100%; text-align: right; margin: 5px 0px 11px 0px; font-size: 10px; font-family: Arial, Helvetica, sans-serif; color: #333; font-size:12px; font-weight: bold;">'.$dialogMemberNumber.' Members  > '.$dialogIdeaNumber.' Ideas</div>
            <div style="height:0px; width: 100%; background-color: #666666;"></div>';

// output the HTML content
//$pdf->writeHTML($html, true, false, true, false, '');

    if ($ReportType == "2")
    {
      $html .='<h2 style="padding: 2px 0px 0px 0px; margin: 0px; font-family: Arial, Helvetica, sans-serif; color: #333; font-size: 21px;">List of ideas & clarifications</h2>';
    }
    else
    {
      $html .= '<h2 style="padding: 2px 0px 0px 0px; margin: 0px; font-family: Arial, Helvetica, sans-serif; color: #333; font-size: 21px;">List of ideas</h2>';

    } 

//$pdf->writeHTML($html, true, false, true, false, '');

$imageString = "http://api.ideaprism.net/uploadImages/1608633748691.jpg";
$creatorName = "Somil jain test";
$title = "Test14";
$clarification = "Dh jfjfjfhffhdhbdjffJ jfjhffhdjffjhhi hfhdhhshsjsjsjs jshshsueHeujshhshsheh bshhsueheue hsueheuehe heueueuehe dh jfjfjfhffhdhbdjffJ jfjhffhdjffjhhi hfhdhhshsjsjsjs jshshsueHeujshhshsheh bshhsueheue gaye hehehe heue weueuehe dh jfjfjfhffhdhbdjffJ jfjhffhdjffjhhi hfhdhhshsjsjsjs jshshsueHeujshhshsheh";
$position=1;

        
    for ($i=1; $i < 7; $i++) 
    {
        
        $position =$i;   
        $html .= '<div style="position:relative;margin-top:30px;">
            <table><tbody><tr>
            <td style="border:1px solid #fff;background:#B3B3B3;min-width:32px;width:32px;text-align:center;"><span style="line-height: 30px; font-family: Arial, Helvetica, sans-serif; color: #6B6B6B; font-size: 15px; font-weight:500;">'.$position.'</td>
            <td style="padding-left: 5px;padding-right: 5px;"><img style ="height:30px; max-width:30px;" src="'.$imageString.'"></td>
            <td style="font-family: Arial, Helvetica, sans-serif; color: #333; font-size: 15px; font-weight: bold;">'.$creatorName.'</td></tr></tbody></table>
            
            <div style="text-align: justify; padding-top: 5px;display: block; width: 100%; font-family: Arial, Helvetica, sans-serif; color: #333; font-size: 15px; font-weight: bold;">'.$title.'</div>'; 
        if ($ReportType == "2")
        {
            $html .= '<div style="background-color: #F2F1F7; color: #333; font-family: Arial,Helvetica,sans-serif; font-size: 13px; font-weight: 600; letter-spacing: 0.1px; line-height: 17px; margin-top: 4px; padding: 8px; text-align: justify;">'.$clarification.'</div>';
        }

        $html .= '</div>';
        
    } 

$footerImageString= "https://ideaprism.net/Images/idea-logo.jpg";

// test some inline CSS
$html .= '<div style="width: 100%; border-top: 2px solid #666666; display: inline-block;margin-top:30px;">
                <div style="width: 90px; float: left; margin-top: 10px;">
                    <img src="'.$footerImageString.'" /></div>
                <div style="width: 455px; float: right; text-align: right; margin-top: 10px; font-size: 11px; float:right;">
                    <span style="width: 100%; font-family: Arial, Helvetica, sans-serif; color: #666666; float:right;">Generated by the participants of the Dialogue</span>
                    <br />
                    <span style="font-family: Arial, Helvetica, sans-serif; color: #666666; float: right; text-align: right;display:none;">Report prepared on
                        <input 
                        type="text" id="start" 
                        style="border: none;
                        width: 60px;
                  font-size:10px;
                        text-align: right;
                        color: #666666;
                        float:right;
                        "
                        /></span>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
</body>
</html>';

// echo $html; die; 

$mpdf = new \Mpdf\Mpdf();

$mpdf->WriteHTML($html);

//$mpdf->Output();

// ---------------------------------------------------------
$filelocation= dirname(__FILE__).'/uploads';
$filename="example_".time().".pdf"; 

$fileNL = $filelocation."/".$filename; //Linux

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$mpdf->Output($fileNL, 'F'); // I, D, F

echo 'PDF Downloaded';